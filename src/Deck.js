import React, {Component} from 'react';
import {
    View,
    Animated,
    PanResponder,
    Dimensions,
    LayoutAnimation,
    UIManager
} from 'react-native';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SWIPE_THRESHOLD = SCREEN_WIDTH * 0.25;
const SWIPE_OUT_DURATION = 250;

class Deck extends Component{

    static defaultProps = {
        /** 
         * if compoenent is not passed some properties, the specified properties below will be used instead.
         * this is a good practice to write reusable compoenents and avoid some errors.
        */
        onSwipeLeft: ()=>{},
        onSwipeRight: ()=>{}
    }

    constructor(props){
        super(props);
        const position = new Animated.ValueXY();
        /** 
         * actually PanRespnder has nothing to the with the state system. 
         * we could also have done: this.panResponder = panResponder;
         * most of the documentation uses this.state assignment to make the responder accessible
         * thoroughout the component. that is the only reason we use this.state here.
         * 
         * we can have multiple PanResponders in a single componenet. each PanResponder instance 
         * will handle the gectures for a specific component.
         * we can also create a single pan responder and attach to different compoenents at different
         * time to handle the gesture for specific components.
         * 
         * questions to answer with PanResponder:
         * 
         * 1- what are we touching?
         * 2- what component handles touch?
         * 3- how is gesture changing?
        */
        const panResponder = PanResponder.create({
            //runs when user touches the screen. this is executed anytime user press down on the screen.
            //returning true says this PanResponder will be responsible for handling that gesture when
            //user place their finger on a card and drag their finger on the screen
            //we can also return/run functions and run a bit of code inside to determine if we want this pan responder
            //to be responsible for a specific gecture instead of a single boolean as below.
            onStartShouldSetPanResponder: ()=>true,
            //runs when user moves/drags their finger on the screen
            //will be called many times as user dragging an element around the screen. basically at each pixel.
            onPanResponderMove: (event,gesture)=>{
                position.setValue({x:gesture.dx, y:gesture.dy});
                //debugger
                //console.log(gesture);
            },
            //runs when users remove their finger from screen
            //usually a good place to do some final tasks
            onPanResponderRelease: (event, gesture)=>{
                if(gesture.dx >= SWIPE_THRESHOLD){
                    console.log("RIGHT");
                    this.forceSwipe('right');
                }else if(gesture.dx <= -SWIPE_THRESHOLD){
                    console.log("LEFT");
                    this.forceSwipe('left');
                }else{
                    this.resetPosition();
                }
            }

        });

        this.state = {panResponder, position, index:0}; 
    }
    /** 
     * whenever compoenent is to be re-rendered with a new set of props, this lifecycle method
     * is called. this is a good place to compare incoming set of props with the existing set 
     * of props we already have. new set of props are set as an argumen to this function. by convention
     * we refer to them as nextProps.
     * If we want to reload a new set of data after we swiped the last card, we need to reset index
     * so that we see the right card at the top, otherwise we see the card with the index tat corresponds
     * to the last incremented index while showing the previous set of cards! now we can successfully 
     * re-render the component with a new set of data.
    */
    componentWillReceiveProps(nextProps){
        /** 
         * here the objects/contents of the arrays are not compared. comparison here asks the 
         * question if they are the exact same array in memory.
        */
        if(nextProps.data!==this.props.data){
            this.setState({index:0});
        }
    }
    componentWillUpdate(){
        /** 
         * below line is for android. check if setLayoutAni.. exists. if it does, run it with "true"
         * as an input parameter.
        */
        UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
        /** 
         * below means that next time this component is re-rendered we want to animate any changes
         * made to the component.
        */
        LayoutAnimation.spring();
    }
    forceSwipe(direction){
        const x = direction === 'right' ? SCREEN_WIDTH : -SCREEN_WIDTH;
        Animated.timing(this.state.position,{
            toValue: {x:x*1.2,y:0}, duration:SWIPE_OUT_DURATION
        }).start(()=>this.onSwipeComplete(direction));//callback function will be executed only after the animation has been completed.
    }
    onSwipeComplete(direction){
        const {onSwipeLeft, onSwipeRight,data} = this.props;
        const item = data[this.state.index];
        direction === 'right' ? onSwipeRight(item) : onSwipeLeft(item);
        /** 
         * reset the position for the next card.
         * 
         * here we are not using setState. as discussed above, it is not very meaningful to put the position inside the 
         * component state but we are following the common approach here. Remember also that is it wrong to mutate state
         * objects without using setState funtions. This is an exceptional case for position object if we put it inside
         * our object state.
        */
        this.state.position.setValue({x:0, y:0});
        /** 
         * swiping a card update the state, updates the component as below (setState) 
         * it causes everything to move upwards (re-render). we add componentWillUpdate lifecycle
         * method to animate any changes accross re-renders. see above.
        */
        this.setState({index: this.state.index + 1});
    }
    resetPosition(){
        Animated.spring(this.state.position,{
            toValue:{x:0,y:0}
        }).start();
    }
    getCardStyle(){
        /** 
         * interpolation system allows us associate one (inout) scale with another (output) scale.
         * multiply Screen_WIDTH by 2 to decrease the rotation. same amount of rotation requires more dragging.
        */
       const {position} = this.state;
       const interpoleRotation = position.x.interpolate({
            inputRange:[-SCREEN_WIDTH*2, 0, SCREEN_WIDTH*2],
            outputRange:['-120deg', '0deg', '120deg']
       });

        return {
            /** 
             * getLayout() returns an object that has some information about Card position.
             * we use spread (...) operator to get all the different properties out of 
             * getLayout() object, add all those properties and tranform property, return all
             * of them as a single object inside this return statement.
            */
            ...position.getLayout(),
            transform: [{rotate: interpoleRotation}]
        };
    }

    renderCards(){
        if(this.state.index >= this.props.data.length){
            return this.props.renderNoMoreCards();
        }
        return this.props.data.map((item,indexx)=>{
            //dont render already swiped cards. just return null for them.
            if(indexx<this.state.index){
                return null;
            }
            //attach PanResponder to the current card
            if(indexx === this.state.index){
                return(
                    /** 
                     * Animated.View is rendered only once. then as the style attribute receives new values, it gets animated
                    */
                    <Animated.View
                    key={item.id}
                    {...this.state.panResponder.panHandlers}
                    style={[this.getCardStyle(), styles.cardStyle(indexx)]}
                    >
                        {this.props.renderCard(item)}
                    </Animated.View>
                );
            }
            //for the rest, just render cards
            //below we use Animated.View instead of View to stop flashing images effect
            //when switching from View to Animated.View for a card.
            //cascading achieved with the "top" styling below.
            return(
                <Animated.View 
                key={item.id} 
                style={[styles.cardStyle(indexx),{top:10*(indexx-this.state.index)}]}
                >
                    {this.props.renderCard(item)}
                </Animated.View>
            );
        }).reverse();
    }
    render(){
        return(
            <Animated.View>
                {this.renderCards()}
            </Animated.View>
        );
    }
}
/** 
 * i variable below is used for styling z-index of individual cards. 
 * this solves the "2nd card shows first" problem on android.
*/
const styles ={
    cardStyle:(i)=>({
        position:'absolute',
        width:SCREEN_WIDTH,
        zIndex: i*-1
    })
};
export default Deck;